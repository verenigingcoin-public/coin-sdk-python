from coin_sdk.mobile_connect.v3.mcconfig import MCConfig
from coin_sdk.mobile_connect.v3.sender import Sender


class QuickstartSender:
    operator = None

    def __init__(self, config: MCConfig):
        self._sender = Sender(config)

    def discovery_request(self, msisdn: str, correlation_id: str):
        discovery_response = self._sender.send_discovery_request(msisdn=msisdn, correlation_id=correlation_id)
        print(discovery_response)
