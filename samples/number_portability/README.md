# COIN Number Portability Python SDK Samples

For extensive documentation on the Python Number Portability SDK, please have a look at the following [README](../../coin_sdk/number_portability/README.md).

## Setup
Make sure you have installed pip and pipenv.

- use your systems package manager to install pip (or refer to https://pip.pypa.io/en/stable/installing/)
- https://docs.pipenv.org/en/latest/#install-pipenv-today

Go to the subfolder `samples/number_portability`. All the required python packages are defined in the Pipfile. To initialize the right environment:
- pipenv shell
- pipenv install
