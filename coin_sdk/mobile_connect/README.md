# Python Mobile Connect SDK

## Introduction

This SDK supports secured access to the mobile connect API.

For a quick start, follow the steps below:
* [Setup](#setup)
* [Configure Credentials](#configure-credentials)
* [Send Messages](#send-messages)


## Setup

### Sample Project for the Mobile Connect' API
- A sample project is provided in the `samples/mobile_connect` directory.


## Configure Credentials

For secure access credentials are required.
- Check [this README](https://gitlab.com/verenigingcoin-public/consumer-configuration/-/blob/master/README.md) to find out how to configure these.
- In summary, you will need:
    - a consumer name
    - a private key file `private-key.pem`
    - a file containing the encrypted Hmac secret `sharedkey.encrypted`

Configure these setting at the bottom of the quickstart.py file: 

## Send Messages
`quickstart.py` shows how to create and send messages. 

When successful (HTTP 200), the `send_discovery_request()` function returns a `DiscoveryResponseV3` object:
```python
class DiscoveryResponseV3:
    networkOperatorCode: str
    supportedServices: SupportedServicesDto
    correlationId: str
    clientId: str
    clientSecret: str
```
When no results are found for the given `msisdn` (HTTP 404),
the `send_discovery_request()` function will return `None`.

When unsuccessful (HTTP != 200), the `send()` function will throw a `requests.HTTPError`.

When applicable, this error contains the error code and description returned by the API.
