# Changelog

## Version 1.6.0
- Updated Python version
  - `python_version = "3.12.8"`

## Version 1.5.0

- changed HMAC encryption to include query parameters since API gateway does not support old method (no query params) anymore.

## Version 1.4.0

- Updated pyjwt to version 2.4.0
- upgraded build / run to python 3.10.5

## Version 1.3.0

Changed:
- Bug fix: replace ConfirmationStatus.CONFIRMED with ConfirmationStatus.UNCONFIRMED

## Version 1.2.0

Added:
- Support for Number Portability v3; adds the contract field to porting requests
