# COIN SDK for Python

[![PyPI version](https://badge.fury.io/py/coin-sdk.svg)](https://badge.fury.io/py/coin-sdk)
[![CI Status](https://gitlab.com/verenigingcoin-public/coin-sdk-python/badges/master/pipeline.svg)](https://gitlab.com/verenigingcoin-public/coin-sdk-python/-/pipelines/latest)

| Api                                                                   | SDK Version   | Api Version                                          | Changelog                           |
|-----------------------------------------------------------------------|---------------|------------------------------------------------------|-------------------------------------|
| [mobile-connect](https://coin.nl/nl/diensten/identity-management) | 1.6.0 +       | [v3](https://api.coin.nl/docs/mobile-connect/v3) | [click](https://gitlab.com/verenigingcoin-public/coin-sdk-python/-/blob/master/CHANGELOG.md) |
| [number-portability](https://coin.nl/en/services/nummerportabiliteit) | 1.2.0 +       | [v3](https://api.coin.nl/docs/number-portability/v3) | [click](https://gitlab.com/verenigingcoin-public/coin-sdk-python/-/blob/master/CHANGELOG.md) |
|                                                                       | 0.1.3 - 1.1.1 | [v1](https://api.coin.nl/docs/number-portability/v1) | -                                   |
| [bundle-switching](https://coin.nl/en/services/overstappen)           | n/a           | n/a                                                  | n/a                                 |


This project contains SDKs for various COIN APIs.
- [Number Portability](https://gitlab.com/verenigingcoin-public/coin-sdk-python/-/blob/master/coin_sdk/number_portability/README.md)
- [Mobile Connect](https://gitlab.com/verenigingcoin-public/coin-sdk-python/-/blob/master/coin_sdk/mobile_connect/README.md)
- For other APIs you can use [Common](https://gitlab.com/verenigingcoin-public/coin-sdk-python/-/tree/master/coin_sdk/common) to add correct credentials to your requests

To use an SDK, you need to [configure a consumer](https://gitlab.com/verenigingcoin-public/consumer-configuration/-/blob/master/README.md).

## Support
If you need support, feel free to send an email to the [COIN devops team](mailto:devops@coin.nl).
