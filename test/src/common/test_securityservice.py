import unittest

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives._serialization import Encoding, PrivateFormat, NoEncryption
from cryptography.hazmat.primitives.asymmetric import rsa

from coin_sdk.common.config import Config
from coin_sdk.common.securityservice import SecurityService


def generate_private_key(encoding=Encoding.PEM):
    key_pair = rsa.generate_private_key(
        backend=default_backend(),
        public_exponent=65537,
        key_size=2048)
    private_key = key_pair.private_bytes(
        encoding,
        PrivateFormat.PKCS8,
        NoEncryption())
    return private_key


class SecurityServiceTest(unittest.TestCase):

    def test_instantiate_with_valid_shared_key_and_in_mem_private_key(self):
        private_key = generate_private_key()

        config = Config(
            base_uri="http://localhost",
            consumer_name="test-consumer",
            private_key_mem=private_key,
            shared_key="123"
        )
        SecurityService(config)

    def test_instantiate_throws_with_private_key_of_the_wrong_type(self):
        with self.assertRaises(Exception) as context:
            config = Config(
                base_uri="http://localhost",
                consumer_name="test-consumer",
                private_key_mem="123",
                shared_key="123"
            )
            SecurityService(config)
        self.assertIsInstance(context.exception, TypeError)

    def test_instantiate_throws_with_private_key_of_the_wrong_encoding(self):
        with self.assertRaises(Exception) as context:
            config = Config(
                base_uri="http://localhost",
                consumer_name="test-consumer",
                private_key_mem=generate_private_key(encoding=Encoding.DER),
                shared_key="123"
            )
            SecurityService(config)
        self.assertIsInstance(context.exception, ValueError)

    def test_instantiate_throws_with_shared_key_in_wrong_format(self):
        with self.assertRaises(Exception) as context:
            config = Config(
                base_uri="http://localhost",
                consumer_name="test-consumer",
                private_key_mem="123",
                shared_key=bytes("123")
            )
            SecurityService(config)
        self.assertIsInstance(context.exception, TypeError)
