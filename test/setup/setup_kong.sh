#!/bin/bash -xv

KONG_ADMIN_URL=${KONG_ADMIN_URL:-$($(dirname $0)/get-kong-url.sh 8001)}
KONG_APP_URL=${KONG_APP_URL:-$($(dirname $0)/get-kong-url.sh 8000)}

kong=${KONG_HOST:-http://localhost:8001}
secret=secret123
username1=loadtest-loada
username2=loadtest-loadb
keys=${KEYS_LOCATION:-.}

echo -n "Creating services..."
service_id_np=$(curl -s -X POST ${kong}/services/ \
	--data 'name=np' \
	--data 'host=np-api-stub' \
	--data 'port=8443' \
	--data 'protocol=https' | jq -r '.id')
service_id_mc=$(curl -s -X POST ${kong}/services/ \
	--data 'name=mc' \
	--data 'host=mc-stub' \
	--data 'port=8443' \
	--data 'protocol=http' | jq -r '.id')
echo " done"

echo -n "Creating routes..."
route_id_np=$(curl -s -X POST ${kong}/routes/ \
	--data 'methods[]=GET' \
	--data 'methods[]=PUT' \
	--data 'methods[]=POST' \
	--data 'paths[]=/number-portability/v3' \
	--data 'strip_path=false' \
	--data "service.id=$service_id_np" | jq -r '.id')
route_id_mc=$(curl -s -X POST ${kong}/routes/ \
	--data 'methods[]=GET' \
	--data 'methods[]=PUT' \
	--data 'methods[]=POST' \
	--data 'paths[]=/mobile-connect' \
	--data 'strip_path=false' \
	--data "service.id=$service_id_mc" | jq -r '.id')
echo " done"

jwt_plugin='{
      "name": "jwt",
      "config": {
	"claims_to_verify": [
	  "exp",
	  "nbf"
	],
	"key_claim_name": "iss",
	"cookie_names": [
	  "jwt"
	],
	"maximum_expiration": 0,
	"secret_is_base64": false
      },
      "protocols": ["http", "https"],
      "run_on": "first"
    }'


stop_stream_service_id=$(curl -s -X POST ${kong}/services/ \
	--data 'name=stopstream' \
	--data 'host=api-stub' \
	--data 'port=8443' \
	--data 'protocol=https' | jq -r '.id')

curl -s -X POST ${kong}/routes/ \
	--data 'methods[]=GET' \
	--data 'paths[]=/number-portability/v3/dossiers/stopstream' \
	--data 'strip_path=false' \
	--data "service.id=$stop_stream_service_id"

echo -n "Setting jwt and hmac plugin..."
curl -o /dev/null -sS -X POST ${kong}/services/${service_id_np}/plugins -H 'Content-Type: application/json' -d "${jwt_plugin}"
curl -o /dev/null -sS -X POST ${kong}/services/${service_id_np}/plugins/ --data "name=hmac-auth"
curl -o /dev/null -sS -X POST ${KONG_ADMIN_URL}/services/${service_id_mc}/plugins -H 'Content-Type: application/json' -d "${jwt_plugin}"
curl -o /dev/null -sS -X POST ${KONG_ADMIN_URL}/services/${service_id_mc}/plugins/ --data "name=hmac-auth"
echo " done"

rm -f "${KEYS_LOCATION}"/{private-key.pem,private-key.pem.pub,public-key.pem,sharedkey.encrypted}
ssh-keygen -m PEM -t rsa -b 4096 -f "${KEYS_LOCATION}/private-key.pem" -N ''
ssh-keygen -e -m PKCS8 -f "${KEYS_LOCATION}/private-key.pem" > "${KEYS_LOCATION}/public-key.pem"
echo -n ${secret} | openssl rsautl -encrypt -inkey ${KEYS_LOCATION}/public-key.pem -pubin -pkcs | base64 | tr -d \\n > "${KEYS_LOCATION}/sharedkey.encrypted"
chmod 0644 "${KEYS_LOCATION}/private-key.pem"

add_consumer() {
	local user="$1"
	curl -o /dev/null -sS -X POST ${kong}/consumers/ --data "username=${user}"
	curl -o /dev/null -sS -X POST ${kong}/consumers/${user}/hmac-auth/ \
		--data "username=${user}" \
		--data "secret=${secret}"

	curl -o /dev/null -sS -X POST ${kong}/consumers/${user}/jwt/ \
		--data-urlencode "key=${user}" \
		--data-urlencode "rsa_public_key=$(<"${KEYS_LOCATION}/public-key.pem")" \
		--data-urlencode 'algorithm=RS256'
}

add_consumer "$username1"
add_consumer "$username2"
