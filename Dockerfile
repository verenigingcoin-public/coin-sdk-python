FROM python:3.12.8

WORKDIR /build

RUN pip install pipenv twine
ADD README.md /build/README.md
ADD Pipfile /build/Pipfile
ADD Pipfile.lock /build/Pipfile.lock
ADD setup.py /build/setup.py
ADD build/.pypirc /root/.pypirc
ADD docker/entrypoint.sh /build/entrypoint.sh

ARG NAME

ADD coin_sdk /build/coin_sdk
ADD test /build/test

RUN pipenv install --dev && \
    pipenv run python setup.py check && \
    pipenv run python setup.py build

ARG VERSION

ENTRYPOINT ["/build/entrypoint.sh"]

CMD ["test"]
